console.log("JS Loaded")

const url = "127.0.0.1:3000"

var inputForm = document.getElementById("inputTweet")
var data = document.getElementById("tweetToPost")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    const formdata = new FormData()
    formdata.append("message", data.value)
    fetch(url,{
        method:"POST",
        body:formdata,
    }).then(
        response => response.text()
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("mainUpdateTopicList")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    const formdata = new FormData(inputForm)
    fetch(url,{
        method:"updateMainTopicList",
        body:formdata,
    })
    .then(
        response => response.text()
    ).then(
        (data) => {document.getElementById("mainTopicList").innerHTML=data}
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("mainInterests")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    arr = []
    const mainTopics = document.getElementsByName("mainTopic")
    for (let i = 0; i < mainTopics.length; i++) {
        if (mainTopics[i].checked)
        arr.push(mainTopics[i].value)
    }

    const formdata = JSON.stringify({
        mainTopics: arr
    })
    fetch(url,{
        method:"updateMainInterests",
        body:formdata,
    }).then(
        response => response.text()
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("archiveUpdateTopicList")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    const formdata = new FormData(inputForm)
    fetch(url,{
        method:"updateArchiveTopicList",
        body:formdata,
    })
    .then(
        response => response.text()
    )
    .then(
        (data) => {document.getElementById("archiveTopicList").innerHTML=data}
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("archiveInterests")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    arr = []
    const archiveTopics = document.getElementsByName("archiveTopic")
    for (let i = 0; i < archiveTopics.length; i++) {
        if (archiveTopics[i].checked)
        arr.push(archiveTopics[i].value)
    }

    const formdata = JSON.stringify({
        archiveTopics: arr
    })
    fetch(url,{
        method:"updateArchiveInterests",
        body:formdata,
    }).then(
        response => response.text()
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("mainTweets")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    const formdata = new FormData(inputForm)
    fetch(url,{
        method:"getTweets",
        body:formdata,
    })
    .then(
        response => response.text()
    ).then(
        (data) => {document.getElementById("displayMainTweets").innerHTML=data}
    ).catch(
        error => console.error(error)
    )
})

var inputForm = document.getElementById("archiveTweets")

inputForm.addEventListener("submit", (e)=>{

    //prevent auto submission
    e.preventDefault()

    const formdata = new FormData(inputForm)
    fetch(url,{
        method:"getArchiveTweets",
        body:formdata,
    })
    .then(
        response => response.text()
    ).then(
        (data) => {document.getElementById("displayArchiveTweets").innerHTML=data}
    ).catch(
        error => console.error(error)
    )
})