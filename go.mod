module 2PPS

go 1.17

require golang.org/x/crypto v0.0.0-20220128200615-198e4374d7ed

require (
	github.com/pkg/profile v1.6.0
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
