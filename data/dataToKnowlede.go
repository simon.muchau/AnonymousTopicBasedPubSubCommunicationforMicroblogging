package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	//calculates avg publisher %
	var pubPerc float64
	var counter float64
	f1, err := os.Open("evalData_500_5_Full_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f1.Close()

	scanner1 := bufio.NewScanner(f1)

	for scanner1.Scan() {
		if strings.Contains(scanner1.Text(), "currentPublisherAmount") {
			counter++
			textArr := strings.Split(scanner1.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-1], 64)
			if err != nil {
				panic(err)
			}
			pubPerc += num
		}
	}

	fmt.Println("pubPercentage", pubPerc/counter)

	//calculates avg db Size

	f2, err := os.Open("evalData_1000_5_278_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f2.Close()

	scanner2 := bufio.NewScanner(f2)

	counter = 0
	var dbSizeAvg float64

	for scanner2.Scan() {
		if strings.Contains(scanner2.Text(), "dbWriteSize") {
			counter++
			textArr := strings.Split(scanner2.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-1], 64)
			if err != nil {
				panic(err)
			}
			dbSizeAvg += num
		}
	}

	fmt.Println("dbSizeAvg", dbSizeAvg/counter)

	f2, err = os.Open("evalData_1000_5_278_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f2.Close()
	scanner3 := bufio.NewScanner(f2)

	counter = 0
	var collPerc float64

	for scanner3.Scan() {
		if strings.Contains(scanner3.Text(), "dbWriteSize") {
			counter++
			textArr := strings.Split(scanner3.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-3], 64)
			if err != nil {
				panic(err)
			}
			collPerc += num
		}
	}

	fmt.Println("collPerc", collPerc/counter)

	f1, err = os.Open("evalData_500_5_Full_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f1.Close()

	scanner4 := bufio.NewScanner(f1)

	counter = 0
	var bytesSavedPerc float64
	row := 0

	for scanner4.Scan() {
		row++
		if row < 680 {
			continue
		}
		if strings.Contains(scanner4.Text(), "bytesSaved") {
			counter++
			textArr := strings.Split(scanner4.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-1], 64)
			if err != nil {
				panic(err)
			}
			bytesSavedPerc += num
		}
	}

	fmt.Println("bytesSavedPerc", bytesSavedPerc/counter)

	f1, err = os.Open("evalDataClient_500_5_Full_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f1.Close()

	scanner5 := bufio.NewScanner(f1)

	counter = 0
	var goodPadding float64

	row = 0
	for scanner5.Scan() {
		row++
		if strings.Contains(scanner5.Text(), "goodPadding Percentage") {
			counter++
			textArr := strings.Split(scanner5.Text(), " ")
			numText := textArr[len(textArr)-1]

			if numText != "NaN" {
				num, err := strconv.ParseFloat(numText, 64)
				if err != nil {
					panic(err)
				}
				goodPadding += num

			}

		}
	}

	fmt.Println("goodPadding", goodPadding/counter)

	f1, err = os.Open("evalData_1000_1_124_NoPad")

	if err != nil {
		log.Fatal(err)
	}

	defer f1.Close()

	scanner6 := bufio.NewScanner(f1)

	f2, err = os.Open("evalData_1000_1_52_Pad")

	if err != nil {
		log.Fatal(err)
	}

	defer f2.Close()

	scanner7 := bufio.NewScanner(f2)

	counter1 := 0
	counter2 := 0

	var phase3Dur1 float64
	var phase3Dur2 float64

	for scanner6.Scan() {
		if strings.Contains(scanner6.Text(), "fullDurationPhase3") {
			counter1++
			textArr := strings.Split(scanner6.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-1], 64)
			if err != nil {
				panic(err)
			}
			phase3Dur1 += num
		}
	}

	for scanner7.Scan() {
		if strings.Contains(scanner7.Text(), "fullDurationPhase3") {
			counter2++
			textArr := strings.Split(scanner7.Text(), " ")
			num, err := strconv.ParseFloat(textArr[len(textArr)-1], 64)
			if err != nil {
				panic(err)
			}
			phase3Dur2 += num
		}
	}

	phase3Avg1 := phase3Dur1 / float64(counter1)
	phase3Avg2 := phase3Dur2 / float64(counter2)

	fmt.Println("avgDuration increase with padding", phase3Avg2/phase3Avg1)
}
