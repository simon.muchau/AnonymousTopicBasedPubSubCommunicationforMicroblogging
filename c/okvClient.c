#include "okvClient.h"
#include <openssl/rand.h>
#include <omp.h>
#include <math.h>

//for rand() mb i should not use this
//people on the internet say its not cryptographically secure
#include <stdlib.h>

//cipher for ciphering
EVP_CIPHER_CTX *ctx;

//position where client will write to
uint128_t position;

void initializeCipher() {

    if(!(ctx = EVP_CIPHER_CTX_new())) 
        printf("errors occured in creating context\n");
    //should use random key for real world application, this is only for testing
    unsigned char *aeskey = (unsigned char*) "0123456789123456";
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, aeskey, NULL))
        printf("errors occured in init\n");
    EVP_CIPHER_CTX_set_padding(ctx, 0);
}

uint128_t prepQuery(int pos, int dbSize, uint8_t *dataToWrite, int dataSize, int *querySize, uint8_t **dpfQueryA, uint8_t **dpfQueryB){

    position = pos;
    *querySize = 1 + 16 + 1 + 18 * 128 + dataSize;
    
    genDPF(ctx, 128, position, dataSize, dataToWrite, dpfQueryA, dpfQueryB);

    return position;
}

void prepAudit(uint8_t *seed, uint8_t *outputsA, uint8_t *outputsB, uint8_t *dpfQueryA, uint8_t *dpfQueryB){
    
    uint128_t shareA;
    uint128_t shareB;
    
    //eval the dpf query at A and B and put results in shareA, shareB
    uint8_t temp[16];
    shareA = evalDPF(ctx, dpfQueryA, position, 16, temp);
    shareB = evalDPF(ctx, dpfQueryB, position, 16, temp);
    
    //call the auditing function
    clientGenProof(ctx, *(uint128_t*)seed, 0, shareA, shareB, outputsA, outputsB);
}
