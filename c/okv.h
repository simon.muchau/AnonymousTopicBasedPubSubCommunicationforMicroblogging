#ifndef _OKV
#define _OKV

#include "dpf.h"

typedef struct{
    EVP_CIPHER_CTX *rowKeyA;
    EVP_CIPHER_CTX *rowKeyB;
    uint128_t newKeyA;
    uint128_t newKeyB;
    int dataSize; //size of the data stored here
    uint8_t* mask; //current mask resulting from rerandomization
    uint8_t* data; //the actual data
} vatRow;

void initializeServer(int numThreads);

void createDb(int isLeader, int dataSize);

void xorIn(int i, uint8_t *data);

void resetDb();

void readData(int index, uint8_t *data);

void readSeed(uint8_t *seedIn);

uint128_t getUint128_t(int i);

void decryptRow(int localIndex, uint8_t *out, uint8_t *dataA, uint8_t *dataB, uint8_t *seedA, uint8_t *seedB);

void getCipher(int isLeader, int i, uint8_t *array);

#endif
