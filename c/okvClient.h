#ifndef _OKVCLIENT
#define _OKVCLIENT

typedef __int128 int128_t;
typedef unsigned __int128 uint128_t;

void initializeCipher();

//prepare a querys
uint128_t prepQuery(int pos, int dbSize, uint8_t *dataToWrite, int dataSize, int *querySize, uint8_t **dpfQueryA, uint8_t **dpfQueryB);

//prepare an audit response for most recent query
void prepAudit(uint8_t *seed, uint8_t *outputsA, uint8_t *outputsB, uint8_t *dpfQueryA, uint8_t *dpfQueryB);

void getCipher(int isLeader, int i, uint8_t *array);

void putCipher(int isLeader, int i, uint8_t *array);

#endif
