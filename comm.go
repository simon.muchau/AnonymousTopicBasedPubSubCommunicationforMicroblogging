package main

import (
	"2PPS/client"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

var Port = ":3000"

func main() {

	f, err := os.OpenFile("evalDataClient", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()

	log.SetOutput(f)
	if os.Args[len(os.Args)-1] == "1" {
		go client.Client(0, f)

	}

	http.HandleFunc("/", ServeFiles)
	fmt.Println("Serving @ : ", "http://127.0.0.1"+Port)
	log.Fatal(http.ListenAndServe(Port, nil))

}

func ServeFiles(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case "GET":

		path := r.URL.Path

		if path == "/" {

			path = "index.html"
		} else {
			path = "/home/simon/goCode/AnonymousTopicBasedPubSubCommunicationforMicroblogging" + path
		}

		http.ServeFile(w, r, path)

	case "POST":

		r.ParseMultipartForm(0)

		tweet := r.FormValue("message")
		fmt.Println("Tweet to post: ", tweet)
		client.SetGuiTweet(tweet)

	case "getTweets":

		r.ParseMultipartForm(0)

		fmt.Fprint(w, client.GetTweets(0))

	case "getArchiveTweets":

		r.ParseMultipartForm(0)

		fmt.Fprint(w, client.GetTweets(1))

	case "updateMainTopicList":

		r.ParseMultipartForm(0)

		fmt.Fprint(w, client.GetTopicList(0))

	case "updateMainInterests":

		var x struct {
			Topics []string `json:"mainTopics"`
		}

		if err := json.NewDecoder(r.Body).Decode(&x); err != nil {
			panic(err)
		}

		topicsToGo := x.Topics
		client.UpdateInterests(topicsToGo, 0)

	case "updateArchiveTopicList":

		r.ParseMultipartForm(0)

		fmt.Fprint(w, client.GetTopicList(1))

	case "updateArchiveInterests":

		var x struct {
			Topics []string `json:"archiveTopics"`
		}

		if err := json.NewDecoder(r.Body).Decode(&x); err != nil {
			panic(err)
		}

		topicsToGo := x.Topics
		client.UpdateInterests(topicsToGo, 1)

	default:
		fmt.Fprintf(w, "Request type not supported")

	}

}
