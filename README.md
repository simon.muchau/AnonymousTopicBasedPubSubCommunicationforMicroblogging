# Anonymous Topic-Based PubSub Communication for Microblogging

This is the source code following the bachelor thesis Anonymous Topic-Based PubSub Communication for Microblogging.

To execute you need to run follower.go, wait for print "start leader", then run leader.go, wait for 1sec and then start client.go
The parameters used, like number of clients or the write database size, are global variables in the respective files.